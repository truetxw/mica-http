# mica-http
[![mica-http Maven](https://img.shields.io/maven-central/v/net.dreamlu/mica-http.svg?style=flat-square)](https://mvnrepository.com/artifact/net.dreamlu/mica-http)

`mica-http` 是基于 `okhttp` 的简单 http 工具包，语法参考 HttpClient Fluent API（fluent-hc）。

[版本更新记录](CHANGELOG.md)

## 使用
### maven
```xml
<dependency>
  <groupId>net.dreamlu</groupId>
  <artifactId>mica-http</artifactId>
  <version>${version}</version>
</dependency>
```

### gradle
```groovy
compile("net.dreamlu:mica-http:${version}")
```

### 示例代码
```java
private String getUserEmail(String accessToken) {
    return XRequest.get("https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))")
            .addHeader("Host", "api.linkedin.com")
            .addHeader("Connection", "Keep-Alive")
            .addHeader("Authorization", "Bearer " + accessToken)
            .execute()
            .asJsonNode()
            .at("/elements/0/handle~/emailAddress")
            .asText();
}
```

## 开源协议
LGPL（[GNU Lesser General Public License](http://www.gnu.org/licenses/lgpl.html)）

## 推荐
加入【如梦技术】QQ群：`479710041`，了解更多。

`mica 微服务框架`：https://github.com/lets-mica/mica

## 微信公众号

![如梦技术](docs/dreamlu-weixin.jpg)

精彩内容每日推荐！
